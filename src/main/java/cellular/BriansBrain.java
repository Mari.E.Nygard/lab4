package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;
 
    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = new CellGrid(
				currentGeneration.numRows(), currentGeneration.numColumns(),
				CellState.ALIVE);

		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
		    }
                }

		currentGeneration = nextGeneration;

		}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState cellState = getCellState(row, col);
		
		int aliveNeighbours = countNeighbors(row, col, CellState.ALIVE);
		
		if (cellState==CellState.ALIVE) {
			return CellState.DYING;
		}
		else if (cellState==CellState.DYING){
			return CellState.DEAD;
		}
        else if (aliveNeighbours == 2 && cellState==CellState.DEAD) {
            return CellState.ALIVE;  
        }
		else {
			return CellState.DEAD;
		}
		
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */

	private int countNeighbors(int row, int col, CellState state) {
		int numNeighbours = 0;
		for (int dx = -1; dx <= 1; dx++) {
			for (int dy = -1; dy <= 1; dy++) {
				if (dx == 0 && dy == 0)
					continue; // samme celle, hopp over
				if (col + dy < 0)
					continue; // utenfor brettet
				if (col + dy >= currentGeneration.numColumns())
					continue; // utenfor brettet
				if (row + dx < 0)
					continue; // utenfor brettet
				if (row + dx >= currentGeneration.numRows())
					continue; // utenfor brettet
				
				// tell levende naboer
				if (currentGeneration.get(row + dx, col + dy) == CellState.ALIVE) {
					numNeighbours++;
				}
			}
		}
		return numNeighbours;


	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}

