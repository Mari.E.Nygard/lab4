package datastructure;

import java.util.List;
import java.util.ArrayList;
import cellular.CellState;

public class CellGrid implements IGrid {
    private List<CellState> cells;
	private int columns;
	private int rows;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.columns = columns;
		this.rows = rows;
		cells = new ArrayList<CellState>(columns * rows);
		for (int i = 0; i < columns * rows; ++i) {
			cells.add(initialState);
		}

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row < 0 || row >= rows)
        throw new IndexOutOfBoundsException();
        if(column < 0 || column >= columns)
        throw new IndexOutOfBoundsException();

        cells.set(coordinateToIndex(row, column), element);
    }
    private int coordinateToIndex(int row, int column) {
        return row + column*rows;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row < 0 || row >= rows)
        throw new IndexOutOfBoundsException();
        if(column < 0 || column >= columns)
        throw new IndexOutOfBoundsException();


        return cells.get(coordinateToIndex(row, column));
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(numRows(), numColumns(), null);
        for (int row = 0; row < rows; row++)
            for (int column = 0; column < columns; column++)
                newGrid.set(row, column, get(row, column));
        // TODO Auto-generated method stub
        return newGrid;
    }
    
}
